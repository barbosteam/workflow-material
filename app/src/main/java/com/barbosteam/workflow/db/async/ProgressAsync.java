package com.barbosteam.workflow.db.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.model.resourse.State;
import com.barbosteam.workflow.service.TaskService;
import com.barbosteam.workflow.service.UserService;

/**
 * Created by Alex on 14.04.2015.
 */
public class ProgressAsync extends AsyncTask<Void, Integer, Void> {

    private Context context;
    private MaterialDialog dialog;
    private MaterialDialog.Builder builder;
    private int count;
    private int index = 0;
    private ProgressDialog progressDialog;

    public ProgressAsync(Context context, int count){
        this.context = context;
        this.count = count;
        builder = new MaterialDialog.Builder(context)
                .title("Заповнення бази данних...")
                .content("Зачекайте.")
                .autoDismiss(false)
                .progress(false, count, true);
    }


    @Override
    protected void onPreExecute() {
        dialog = builder.show();
    }

    @Override
    protected void onPostExecute(Void v) {
        dialog.setContent("Готово.");
         if (dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    protected  Void doInBackground(Void... params) {
        UserService userDao = new UserService(context);
        TaskService taskDao = new TaskService(context);
        int k = 1;
        String state = "";
        String dateFinish = "";

        userDao.save(new User("Alex", "email", "phone"));
        userDao.save(new User("Kate", "email", "phone"));
        userDao.save(new User("Mary", "email", "phone"));
        userDao.save(new User("Andy", "email", "phone"));
        userDao.save(new User("Serg", "email", "phone"));
        for (int i = 1; i <= count/5; i++) {
            switch (k){
                case 1:
                    state = State.COMPLETE;
                    dateFinish = "27.04.2015";
                    k++;
                    break;
                case 2:
                    state = State.NOT_COMPLETE;
                    dateFinish = null;
                    k++;
                    break;
                case 3:
                    state = State.PERFORMED;
                    dateFinish = null;
                    k = 1;
                    break;
            }
            taskDao.save(new Task(1, "Alex", "21.04.2015", "27.05.2015",
                    dateFinish, "Topic" + i, "Comment", null, state));
            publishProgress();
            taskDao.save(new Task(2, "Kate", "20.04.2015", "30.05.2015",
                    dateFinish, "Topic" + i, "Comment", null, state));
            publishProgress();
            taskDao.save(new Task(3, "Mary", "20.04.2015", "28.05.2015",
                    dateFinish, "Topic" + i, "Comment", null, state));
            publishProgress();
            taskDao.save(new Task(4, "Andy", "19.04.2015", "29.05.2015",
                    dateFinish, "Topic" + i, "Comment", null, state));
            publishProgress();
            taskDao.save(new Task(5, "Serg", "21.04.2015", "27.05.2015",
                    dateFinish, "Topic" + i, "Comment", null, state));
            publishProgress();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... value) {
        dialog.incrementProgress(1);
    }

}
