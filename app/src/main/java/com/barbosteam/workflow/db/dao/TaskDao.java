package com.barbosteam.workflow.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.barbosteam.workflow.db.dao.core.Dao;
import com.barbosteam.workflow.db.resource.Resource;
import com.barbosteam.workflow.dialog.core.DateToday;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.resourse.State;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public class TaskDao implements Dao<Task> {

    private SQLiteDatabase sqLiteDatabase;

    public TaskDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }


    @Override
    public long save(Task task) {
        return sqLiteDatabase.insert(Resource.Task.TABLE_NAME, null, fullTaskContentValue(task));
    }

    @Override
    public long delete(Task task) {
        return sqLiteDatabase.delete(Resource.Task.TABLE_NAME, Resource.Task.ID + "= ?",
                new String[]{String.valueOf(task.getId())});
    }

    @Override
    public long delete(long id) {
        return sqLiteDatabase.delete(Resource.Task.TABLE_NAME, Resource.Task.ID + "= ?",
                new String[]{String.valueOf(id)});
    }

    @Override
    public int update(final Task task) {
        return sqLiteDatabase.update(Resource.Task.TABLE_NAME, fullTaskContentValue(task),
                Resource.Task.ID + "= ?", new String[]{String.valueOf(task.getId())});
    }

    @Override
    public List<Task> getAll() {
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Resource.Task.TABLE_NAME, null);
        return parseCursor(cursor);
    }

    @Override
    public Task getById(long id) {
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Resource.Task.TABLE_NAME
                + " WHERE " + Resource.Task.ID + "= ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<Task> parseCursor(Cursor cursor) {
        List<Task> taskList = new ArrayList<>();
        if (cursor != null && cursor.moveToLast()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(Resource.Task.ID));
                long idUser = cursor.getLong(cursor.getColumnIndex(Resource.Task.ID_USER));
                String name = cursor.getString(cursor.getColumnIndex(Resource.Task.NAME));
                String dateCreate = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_CREATE));
                String dateComplete = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_COMPLETE));
                String dateFinish = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_FINISH));
                String topic = cursor.getString(cursor.getColumnIndex(Resource.Task.TOPIC));
                String commentYou = cursor.getString(cursor.getColumnIndex(Resource.Task.COMMENT_YOU));
                String commentWork = cursor.getString(cursor.getColumnIndex(Resource.Task.COMMENT_WORK));
                String state = cursor.getString(cursor.getColumnIndex(Resource.Task.STATE));

                if (updateState(String.valueOf(id), state, dateComplete))
                    state = State.NOT_COMPLETE;

                taskList.add(new Task(id, idUser, name, dateCreate, dateComplete,
                        dateFinish, topic, commentYou, commentWork, state));

            } while (cursor.moveToPrevious());

        }

        if (cursor != null) {
            cursor.close();
        }

        return taskList;
    }

    public List<Task> getFilterList(LinkedHashMap<String, String> param){
        List<Task> taskList = new ArrayList<Task>();
        String from = "", to = "", and =" AND ",
                beginQuery = "SELECT * FROM " + Resource.Task.TABLE_NAME,
                where = " WHERE ";
        int count = 1;
        int size = 0;
        boolean dateParam = false;

        if (param != null) {

            size = param.size();

            if (param.get("FROM") != null) {
                Log.e("FROM", param.get("FROM"));
                Log.e("TO", param.get("TO"));
                dateParam = true;
                size--;
                size--;
                from = param.get("FROM");
                to = param.get("TO");
            }

            if (size > 0) {
                beginQuery += where;
            }

            if (param.get(Resource.Task.NAME) != null) {
                Log.e("Resource.Task.NAME", param.get(Resource.Task.NAME));
                beginQuery += Resource.Task.NAME + " = '" + param.get(Resource.Task.NAME) + "'";
                if (size > 1) {
                    beginQuery += and;
                    size--;
                }
            }
            if (param.get(Resource.Task.TOPIC) != null) {
                Log.e("Resource.Task.TOPIC", param.get(Resource.Task.TOPIC));
                beginQuery += Resource.Task.TOPIC + " = '" + param.get(Resource.Task.TOPIC) + "'";
                if (size > 1) {
                    beginQuery += and;
                    size--;
                }
            }
            if (param.get(Resource.Task.STATE) != null) {
                Log.e("Resource.Task.STATE", param.get(Resource.Task.STATE));
                beginQuery += Resource.Task.STATE + " = '" + param.get(Resource.Task.STATE) + "'";
                if (size > 1) {
                    beginQuery += and;
                    size--;
                }
            }
        }


        Cursor cursor = sqLiteDatabase.rawQuery(beginQuery , null);

        if (cursor != null && cursor.moveToLast()) {
            do {
                if (dateParam) {
                    String dateComplete = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_COMPLETE));
                    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    try {
                        Date date = dateFormat.parse(dateComplete);
                        Date fromDate = dateFormat.parse(from);
                        Date toDate = dateFormat.parse(to);

                        if ((date.after(fromDate) && date.before(toDate))
                                || date.equals(fromDate) || date.equals(toDate)) {
                            taskList.add(parseCursorToTask(cursor));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    taskList.add(parseCursorToTask(cursor));
                }


            } while (cursor.moveToPrevious());
        }

        cursor.close();
        return taskList;
    }

    public int[] getStatisticList(LinkedHashMap<String, String> param){
        int complete = 0;
        int notComplete = 0;
        int performed = 0;
        String from = "", to = "", and =" AND ",
                beginQuery = "SELECT * FROM " + Resource.Task.TABLE_NAME,
                where = " WHERE ";
        int size = param.size();
        boolean fromParam = false, toParam = false, fromAndToParam = false;

        dateLabel:
        if (param.get("FROM") != null || param.get("TO") != null) {
            if(param.get("FROM") != null && param.get("TO") != null) {
                from = param.get("FROM");
                to = param.get("TO");
                size--;
                size--;
                fromAndToParam = true;
                break dateLabel;
            }
            if(param.get("FROM") != null) {
                Log.d("Debug", "param.get(FROM) != null");
                from = param.get("FROM");
                size--;
                fromParam = true;
            }
            if(param.get("TO") != null) {
                to = param.get("TO");
                size--;
                toParam = true;
            }
        }

        if (size > 0){
            beginQuery += where;
        }

        if (param.get(Resource.Task.NAME) != null) {
            beginQuery += Resource.Task.NAME + " = '" + param.get(Resource.Task.NAME) + "'";
//            if (size > 1) {
//                beginQuery += and;
//                size--;
//            }
        }

        Cursor cursor = sqLiteDatabase.rawQuery(beginQuery , null);

        if (cursor != null && cursor.moveToLast()) {
            ArrayList<String> res = new ArrayList<>();
            do {
                if (fromParam || toParam || fromAndToParam) {
                    String dateComplete = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_COMPLETE));
                    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    try {
                        Date date = dateFormat.parse(dateComplete);
                        if(fromAndToParam) {
                            Date fromDate = dateFormat.parse(from);
                            Date toDate = dateFormat.parse(to);
                            if((date.after(fromDate) || date.equals(fromDate)) && (date.before(toDate) || date.equals(toDate))) {
                                res.add(cursor.getString(cursor.getColumnIndex(Resource.Task.STATE)));
                            }
                        } else if(fromParam) {
                            Date fromDate = dateFormat.parse(from);
                            if(date.after(fromDate) || date.equals(fromDate)) {
                                res.add(cursor.getString(cursor.getColumnIndex(Resource.Task.STATE)));
                            }
                        } else {
                            Date toDate = dateFormat.parse(to);
                            if(date.before(toDate) || date.equals(toDate)) {
                                res.add(cursor.getString(cursor.getColumnIndex(Resource.Task.STATE)));
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    res.add(cursor.getString(cursor.getColumnIndex(Resource.Task.STATE)));
                }
            } while (cursor.moveToPrevious());

            for(String temp: res) {
                switch (temp){
                    case State.COMPLETE:
                        complete++;
                        break;
                    case State.NOT_COMPLETE:
                        notComplete++;
                        break;
                    case State.PERFORMED:
                        performed++;
                        break;
                }
            }
        }

        cursor.close();
        return new int[]{complete, notComplete, performed};
    }

    private boolean updateState(String id, String state, String dateComplete){
        Date  date = null, dateToday = null;
        ContentValues notComplete = new ContentValues();
        notComplete.put(Resource.Task.STATE, State.NOT_COMPLETE);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            date = dateFormat.parse(dateComplete);
            dateToday = dateFormat.parse(DateToday.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date.before(dateToday) && !state.equals(State.COMPLETE)) {
            sqLiteDatabase.update(Resource.Task.TABLE_NAME, notComplete,
                    Resource.Task.ID + " = ?", new String[]{id});
            return true;
        }
        return false;
    }

    private ContentValues fullTaskContentValue(Task task) {
        ContentValues cv = new ContentValues();
        cv.put(Resource.Task.ID_USER, task.getIdUser());
        cv.put(Resource.Task.NAME, task.getName());
        cv.put(Resource.Task.DATE_CREATE, task.getDateCreate());
        cv.put(Resource.Task.DATE_COMPLETE, task.getDateComplete());
        cv.put(Resource.Task.DATE_FINISH, task.getDateFinish());
        cv.put(Resource.Task.TOPIC, task.getTopic());
        cv.put(Resource.Task.COMMENT_YOU, task.getCommentYou());
        cv.put(Resource.Task.COMMENT_WORK, task.getCommentUser());
        cv.put(Resource.Task.STATE, task.getState());

        return cv;
    }

    private Task parseCursorToTask(Cursor cursor){
        long id = cursor.getLong(cursor.getColumnIndex(Resource.Task.ID));
        long idUser = cursor.getLong(cursor.getColumnIndex(Resource.Task.ID_USER));
        String name = cursor.getString(cursor.getColumnIndex(Resource.Task.NAME));
        String dateCreate = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_CREATE));
        String dateComplete = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_COMPLETE));
        String dateFinish = cursor.getString(cursor.getColumnIndex(Resource.Task.DATE_FINISH));
        String topic = cursor.getString(cursor.getColumnIndex(Resource.Task.TOPIC));
        String commentYou = cursor.getString(cursor.getColumnIndex(Resource.Task.COMMENT_YOU));
        String commentWork = cursor.getString(cursor.getColumnIndex(Resource.Task.COMMENT_WORK));
        String state = cursor.getString(cursor.getColumnIndex(Resource.Task.STATE));

        if (updateState(String.valueOf(id), state, dateComplete))
            state = State.NOT_COMPLETE;

        return new Task(id, idUser, name, dateCreate, dateComplete,
                dateFinish, topic, commentYou, commentWork, state);
    }
}

