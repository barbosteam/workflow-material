package com.barbosteam.workflow.db.dao.core;

import android.database.Cursor;

import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public interface Dao<T> {
    long save(T t);

    long delete(T t);

    long delete(long id);

    int update(T t);

    List<T> getAll();

    T getById(long id);

    List<T> parseCursor(Cursor cursor);

}
