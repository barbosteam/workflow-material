package com.barbosteam.workflow.db.async;

import android.content.Context;
import android.os.AsyncTask;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barbosteam.workflow.fragments.FilterListFragment;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.service.TaskService;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 06.04.2015.
 */
public class FilterFragmentAsync extends AsyncTask<Object, List<Task>, List<Task>> {

    private Context context;
    private FilterListFragment filterListFragment;
    private MaterialDialog waitingDialog;
    private MaterialDialog.Builder builder;
    private LinkedHashMap<String, String> param;

    public FilterFragmentAsync(FilterListFragment filterListFragment, LinkedHashMap<String, String> param){
        this.filterListFragment = filterListFragment;
        this.context = filterListFragment.getActivity();
        this.param = param;
        builder = new MaterialDialog.Builder(context)
                .title("З'єднання...")
                .content("Підключення до бази данних...")
                .progress(true, 1)
                .cancelable(false);
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = builder.show();
    }

    @Override
    protected void onPostExecute(List<Task> taskList) {
        filterListFragment.updateAdapter(taskList);
        if (waitingDialog.isShowing())
            waitingDialog.dismiss();
    }

    @Override
    protected  List<Task> doInBackground(Object... params) {
        return new TaskService(context).getFilterList(param);
    }

}
