package com.barbosteam.workflow.db.async;

import android.content.Context;
import android.os.AsyncTask;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barbosteam.workflow.fragments.MainFragment;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.List;

/**
 * Created by Alex on 20.04.2015.
 */
public class DropDownAsync extends AsyncTask<Void, Void, List<String>> {

    private Context context;
    private MainFragment mainFragment;
    private MaterialDialog waitingDialog;
    private MaterialDialog.Builder builder;
    private MaterialAutoCompleteTextView topic;
    private List<String> list;

    public DropDownAsync(Context context, MaterialAutoCompleteTextView topic){
        this.context = context;
        this.topic = topic;
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = new MaterialDialog.Builder(context)
                .title("З'єднання...")
                .content("Підключення до бази данних...")
                .progress(true, 1)
                .cancelable(false).show();
    }

    @Override
    protected void onPostExecute(List<String> list) {
        //new TopicAdapter(context, list,  topic);
        if (waitingDialog.isShowing())
            waitingDialog.dismiss();
    }

    @Override
    protected  List<String> doInBackground(Void... params) {
        //return new TaskService(context).getTopic();
        return null;
    }

    public List<String> getList() {
        return list;
    }

}