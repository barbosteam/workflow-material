package com.barbosteam.workflow.db.resource;

/**
 * Created by Alex on 23.03.2015.
 */
public class Resource {

    public static final String DATABASE_NAME = "work_flow.db";
    public static final int DATABASE_VERTION = 1;

    public static final class User {

        public static final String TABLE_NAME = "user";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String EMAIL = "email";
        public static final String PHONE = "phone";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, "
                + NAME + " text(255), " + EMAIL + " text(255), " + PHONE + " text(255));";

    }

    public static final class Task {

        public static final String TABLE_NAME = "task";
        public static final String ID = "id";
        public static final String ID_USER = "id_user";
        public static final String NAME = "name";
        public static final String DATE_CREATE = "date_create";
        public static final String DATE_COMPLETE = "date_complete";
        public static final String DATE_FINISH = "date_finish";
        public static final String TOPIC = "topic";
        public static final String COMMENT_YOU = "comment_you";
        public static final String COMMENT_WORK = "comment_work";
        public static final String STATE = "state";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, "
                + ID_USER + " text(255), " + NAME + " text(255), "
                + DATE_CREATE + " text(255), " + DATE_COMPLETE + " text(255), "
                + DATE_FINISH + " text(255), " + TOPIC + " text(255), "
                + COMMENT_YOU + " text(255), " + COMMENT_WORK + " text(255), "
                + STATE + " text(255));";

    }

}
