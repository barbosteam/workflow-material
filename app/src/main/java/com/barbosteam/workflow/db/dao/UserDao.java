package com.barbosteam.workflow.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.barbosteam.workflow.db.dao.core.Dao;
import com.barbosteam.workflow.db.resource.Resource;
import com.barbosteam.workflow.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public class UserDao implements Dao<User> {

    private  SQLiteDatabase sqLiteDatabase;

    public UserDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }


    @Override
    public long save(User user) {
        return sqLiteDatabase.insert(Resource.User.TABLE_NAME, null, fullUserContentValue(user));
    }

    @Override
    public long delete(User user) {
        return sqLiteDatabase.delete(Resource.User.TABLE_NAME, Resource.User.ID + "= ?",
                new String[]{String.valueOf(user.getId())});
    }

    @Override
    public long delete(long id) {
        return sqLiteDatabase.delete(Resource.User.TABLE_NAME, Resource.User.ID + "= ?",
                new String[]{String.valueOf(id)});
    }

    @Override
    public int update(User user) {
        return sqLiteDatabase.update(Resource.User.TABLE_NAME, fullUserContentValue(user),
                Resource.User.ID + "= ?", new String[]{String.valueOf(user.getId())});
    }

    @Override
    public List<User> getAll() {
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Resource.User.TABLE_NAME, null);
        return parseCursor(cursor);
    }

    @Override
    public User getById(long id) {
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Resource.User.TABLE_NAME
                + " WHERE " + Resource.User.ID + "= ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<User> parseCursor(Cursor cursor) {
        List<User> userList = new ArrayList<>();
        if (cursor != null && cursor.moveToLast()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(Resource.User.ID));
                String name = cursor.getString(cursor.getColumnIndex(Resource.User.NAME));
                String email = cursor.getString(cursor.getColumnIndex(Resource.User.EMAIL));
                String phone = cursor.getString(cursor.getColumnIndex(Resource.User.PHONE));

                userList.add(new User(id, name, email, phone));

            } while (cursor.moveToPrevious());

        }
        if (cursor != null) {
            cursor.close();
        }
        return userList;
    }

    public boolean exist(User user) {
        boolean res = false;
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Resource.User.TABLE_NAME
                + " WHERE " + Resource.User.NAME + " = ?", new String[]{user.getName()});
        if(cursor != null) {
            res = parseCursor(cursor).size() > 0;
        }
        return res;
    }

    private ContentValues fullUserContentValue(User user) {
        ContentValues cv = new ContentValues();

        cv.put(Resource.User.NAME, user.getName());
        cv.put(Resource.User.EMAIL, user.getEmail());
        cv.put(Resource.User.PHONE, user.getPhone());

        return cv;
    }
}

