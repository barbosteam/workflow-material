package com.barbosteam.workflow.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.barbosteam.workflow.db.resource.Resource;

/**
 * Created by Alex on 23.03.2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private Context mContext;

    public DBHelper(Context context) {
        super(context, Resource.DATABASE_NAME, null, Resource.DATABASE_VERTION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Resource.User.CREATE_TABLE);
        db.execSQL(Resource.Task.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
