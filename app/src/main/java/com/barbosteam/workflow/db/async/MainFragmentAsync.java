package com.barbosteam.workflow.db.async;

import android.content.Context;
import android.os.AsyncTask;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barbosteam.workflow.fragments.MainFragment;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.service.TaskService;

import java.util.List;

/**
 * Created by Alex on 03.04.2015.
 */
public class MainFragmentAsync extends AsyncTask<Object, List<Task>, List<Task>> {

    private Context context;
    private MainFragment mainFragment;
    private MaterialDialog waitingDialog;
    private MaterialDialog.Builder builder;

    public MainFragmentAsync(MainFragment mainFragment){
        this.mainFragment = mainFragment;
        this.context = mainFragment.getActivity();
        builder = new MaterialDialog.Builder(context)
                .title("З'єднання...")
                .content("Підключення до бази данних...")
                .progress(true, 1)
                .cancelable(false);
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = builder.show();
    }

    @Override
    protected void onPostExecute(List<Task> taskList) {
        mainFragment.updateAdapter(taskList);
        if (waitingDialog.isShowing())
        waitingDialog.dismiss();
    }

    @Override
    protected  List<Task> doInBackground(Object... params) {
        return new TaskService(context).getAll();
    }

}
