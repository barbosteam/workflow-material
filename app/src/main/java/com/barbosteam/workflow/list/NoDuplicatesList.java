package com.barbosteam.workflow.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Alex on 30.03.2015.
 */
public class NoDuplicatesList {
    
    public static ArrayList<String> getList(ArrayList<String> list){
        HashSet<String> hs = new HashSet<>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        Collections.sort(list);
        return list;
    }
}
