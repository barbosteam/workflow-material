package com.barbosteam.workflow.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.adapter.RecyclerAdapter;
import com.barbosteam.workflow.dialog.ColorChooserDialog;
import com.barbosteam.workflow.fragments.FilterFragment;
import com.barbosteam.workflow.fragments.MainFragment;
import com.barbosteam.workflow.fragments.StatisticFragment;
import com.barbosteam.workflow.fragments.TaskCreateFragment;
import com.barbosteam.workflow.fragments.UserAddFragment;
import com.barbosteam.workflow.fragments.UserListFragment;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.service.TaskService;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements ColorChooserDialog.Callback {

    private Toolbar toolbar;
    private Drawer.Result drawerResult;
    private RecyclerAdapter finalRecyclerAdapter;
    private int index = 0, color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
/*
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        String possibleEmail = null;
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;

            }
        }
*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Drawer drawer = new Drawer();
        drawer.withActivity(this);
        drawer.withToolbar(toolbar);
        drawer.withActionBarDrawerToggle(true);
        drawer.withHeaderDivider(false);
        drawer.withHeader(R.layout.drawer_header);
        drawer.withDrawerWidthDp(300);
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.main_display).withIcon(FontAwesome.Icon.faw_home));
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.create_new_task).withIcon(FontAwesome.Icon.faw_tasks));
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.user_add).withIcon(FontAwesome.Icon.faw_user_plus));
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.user_list).withIcon(FontAwesome.Icon.faw_users));
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.filter).withIcon(FontAwesome.Icon.faw_filter));
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.statistic).withIcon(FontAwesome.Icon.faw_pie_chart));
        drawer.addDrawerItems(new DividerDrawerItem());
        drawer.addDrawerItems(new PrimaryDrawerItem().withName(R.string.theme).withIcon(FontAwesome.Icon.faw_adjust));
        drawer.withSelectedItem(0);
        drawer.withOnDrawerListener(new Drawer.OnDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }
        });

        drawer.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            Fragment fragment = null;
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                switch (i){
                    case 1:
                        fragment = new MainFragment();
                        break;
                    case 2:
                        fragment = new TaskCreateFragment();
                        break;
                    case 3:
                        fragment = new UserAddFragment();
                        break;
                    case 4:
                        fragment = new UserListFragment();
                        break;
                    case 5:
                        fragment = new FilterFragment();
                        break;
                    case 6:
                        fragment = new StatisticFragment();
                        break;
                    case 8:
                        new ColorChooserDialog().show(MainActivity.this, index);
                        break;
                }
                if (fragment != null)
                Transaction.showFragment(MainActivity.this, fragment);
            }
        });

        drawerResult = drawer.build();

        String line = readFromFile();
        if (line != null) {
            String[] lines = line.split("\\s");
            color = Integer.valueOf(lines[0]);
            index = Integer.valueOf(lines[1]);
            setAppTheme(index);
            ThemeSingleton.get().neutralColor = color;
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        } else {
            ThemeSingleton.get().neutralColor = getResources().getColor(R.color.indigo);
            getSupportActionBar().setBackgroundDrawable(
                    new ColorDrawable(getResources().getColor(R.color.indigo)));
        }

        Transaction.showFragment(MainActivity.this, new MainFragment());

    }

    @Override
    public void onBackPressed() {
        if (drawerResult.isDrawerOpen()) {
            drawerResult.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public void setSelection(int position){
        drawerResult.setSelection(position - 1);
    }

    public RecyclerAdapter getFinalRecyclerAdapter() {
        return finalRecyclerAdapter;
    }

    public void setFinalRecyclerAdapter(RecyclerAdapter finalRecyclerAdapter) {
        this.finalRecyclerAdapter = finalRecyclerAdapter;
    }

    public void removeTasks(){
        finalRecyclerAdapter = new RecyclerAdapter(MainActivity.this, new TaskService(this).getAll());
    }

    public void removeItem(int position) {
        finalRecyclerAdapter.removeAt(position);
    }

    public void addItem(Task task, int position) {
        if (finalRecyclerAdapter == null)
            finalRecyclerAdapter = new RecyclerAdapter(MainActivity.this, new ArrayList<Task>());
        finalRecyclerAdapter.addAt(task, position);
    }

    @Override
    public void onColorSelection(int index, int color, int darker) {
        this.index = index;
        writeToFile(color, index);
        setAppTheme(index);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        ThemeSingleton.get().neutralColor = color;
        Transaction.refresh(MainActivity.this);
    }

    private void writeToFile(int color, int index) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("color.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(String.valueOf(color) + " " + String.valueOf(index));
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("color.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                    stringBuilder.append(" ");
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            ret = null;
        } catch (IOException e) {
            ret = null;
        }

        return ret;
    }

    public int getIndex(){
        return index;
    }

    public void setAppTheme(int index){
        switch (index){
            case 0:
                super.setTheme(R.style.AppTheme);
                break;
            case 1:
                super.setTheme(R.style.Theme1);
                break;
            case 2:
                super.setTheme(R.style.Theme2);
                break;
            case 3:
                super.setTheme(R.style.Theme3);
                break;
            case 4:
                super.setTheme(R.style.Theme4);
                break;
            case 5:
                super.setTheme(R.style.Theme5);
                break;
            case 6:
                super.setTheme(R.style.Theme6);
                break;
            case 7:
                super.setTheme(R.style.Theme7);
                break;
            case 8:
                super.setTheme(R.style.Theme8);
                break;
            case 9:
                super.setTheme(R.style.Theme9);
                break;
            case 10:
                super.setTheme(R.style.Theme10);
                break;
            case 11:
                super.setTheme(R.style.Theme11);
                break;
            case 12:
                super.setTheme(R.style.Theme12);
                break;
            case 13:
                super.setTheme(R.style.Theme13);
                break;
            case 14:
                super.setTheme(R.style.Theme14);
                break;
            case 15:
                super.setTheme(R.style.Theme15);
                break;
            case 16:
                super.setTheme(R.style.Theme16);
                break;
            case 17:
                super.setTheme(R.style.Theme17);
                break;
            case 18:
                super.setTheme(R.style.Theme18);
                break;
            case 19:
                super.setTheme(R.style.Theme19);
                break;
            case 20:
                super.setTheme(R.style.Theme20);
                break;
        }
    }
}
