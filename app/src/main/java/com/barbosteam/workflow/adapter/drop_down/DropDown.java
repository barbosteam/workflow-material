package com.barbosteam.workflow.adapter.drop_down;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.ArrayList;

/**
 * Created by Alex on 30.03.2015.
 */
public class DropDown {

    private Context context;
    private MaterialAutoCompleteTextView view;
    private CheckBox check;


    DropDown(Context context, ArrayList<String> list, MaterialAutoCompleteTextView view){
        this.context = context;
        this.view = view;
        view.setAdapter(getAdapter(list));
        setListener();
    }
    DropDown(Context context, ArrayList<String> list, MaterialAutoCompleteTextView view, CheckBox check){
        this.context = context;
        this.view = view;
        this.check = check;
        view.setAdapter(getAdapter(list));
        setCheckListener();
        checkClick();
    }

    void checkClick(){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked())
                    view.showDropDown();
                else
                    view.setText(null);
            }
        });
    }

    void setListener(){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view.requestFocus();
                view.showDropDown();
                return false;
            }
        });
    }

    void setCheckListener(){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view.requestFocus();
                view.showDropDown();
                check.setChecked(true);
                return false;
            }
        });
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    if (view.getText().toString().equals(""))
                        check.setChecked(false);
                }
            }
        });
    }

    ArrayAdapter<String> getAdapter(ArrayList<String> list){
        return new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, list);
    }


}
