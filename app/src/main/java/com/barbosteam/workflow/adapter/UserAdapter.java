package com.barbosteam.workflow.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.barbosteam.workflow.R;
import com.barbosteam.workflow.model.User;

import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public class UserAdapter extends BaseAdapter {

    private List<User> userList;
    LayoutInflater inflater;

    private class ViewHolder {
        TextView name;
        TextView email;
        TextView phone;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    public UserAdapter(List<User> userModelList, Activity activity) {
        this.userList = userModelList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return userList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_item, null, false);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.email = (TextView) view.findViewById(R.id.email);
            holder.phone = (TextView) view.findViewById(R.id.phone);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        User userModel = (User) getItem(position);
        if (userModel != null) {
            holder.name.setText(userModel.getName());
            holder.email.setText(userModel.getEmail());
            holder.phone.setText(userModel.getPhone());
        }
        return view;
    }
}
