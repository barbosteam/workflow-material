package com.barbosteam.workflow.adapter.drop_down;

import android.content.Context;
import android.widget.CheckBox;

import com.barbosteam.workflow.model.resourse.State;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 30.03.2015.
 */
public class StateAdapter extends DropDown {

    public StateAdapter(Context context, MaterialAutoCompleteTextView view){
        super(context, State.STATE_LIST, view);
    }

    public StateAdapter(Context context, MaterialAutoCompleteTextView view, CheckBox check){
        super(context, State.STATE_LIST, view, check);
    }

}
