package com.barbosteam.workflow.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.dialog.DateFinishDialog;
import com.barbosteam.workflow.dialog.ShowTaskViewDialog;
import com.barbosteam.workflow.fragments.TaskEditFragment;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.list.NoDuplicatesList;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.resourse.State;
import com.barbosteam.workflow.service.TaskService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    LayoutInflater inflater;
    List<Task> list;
    OnItemLongClickListener onItemClickListener;
    private ViewHolder holder;
    private MainActivity context;

    public RecyclerAdapter(MainActivity context, List<Task> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.adapter_recycler, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        this.holder = holder;
        Task task = list.get(position);
        if (task != null) {
            String state = task.getState();
            if (state.equals(State.COMPLETE))
                holder.circle.setBackgroundResource(R.drawable.circle_complete);
            if (state.equals(State.NOT_COMPLETE))
                holder.circle.setBackgroundResource(R.drawable.circle_not_complete);
            if (state.equals(State.PERFORMED))
                holder.circle.setBackgroundResource(R.drawable.circle_perfomed);

            holder.name.setText(task.getName());
            holder.topic.setText(task.getTopic());
            holder.dateFinish.setText(task.getDateFinish());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Task> getList() {
        return list;
    }

    public ArrayList<String> getNames() {
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
            names.add(list.get(i).getName());
        return NoDuplicatesList.getList(names);
    }

    public ArrayList<String> getTopics() {
        ArrayList<String> topics = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
            topics.add(list.get(i).getTopic());
        return NoDuplicatesList.getList(topics);
    }

    public void removeAt(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

    public void addAt(Task task, int position) {
        list.add(position, task);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, list.size());
    }

    public void setOnItemClickListener(OnItemLongClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{

        TextView name, topic, dateFinish;
        CardView card;
        View circle;


        public ViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            topic = (TextView) itemView.findViewById(R.id.topic);
            dateFinish =(TextView) itemView.findViewById(R.id.date_finish);

            circle = (View) itemView.findViewById(R.id.circle);
            card = (CardView) itemView.findViewById(R.id.card_view);
            card.setCardBackgroundColor(Color.WHITE);

            itemView.setOnLongClickListener(this);
            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            card.setCardBackgroundColor(Color.parseColor("#E1F5FE"));
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            card.setCardBackgroundColor(Color.WHITE);
                            break;
                    }
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Task task = list.get(getPosition());
                    View view = new ShowTaskViewDialog(context, task).getView();
                    new AlertDialogWrapper.Builder(context)
                            .setView(view)
                            .setTitle(context.getString(R.string.task_show))
                            .setPositiveButton(context.getString(R.string.more),
                                    new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    perfomeClick();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton(context.getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });

        }

        public void perfomeClick(){
            itemView.performLongClick();
        }

        @Override
        public boolean onLongClick(View v) {
            CharSequence[] item = null;
            final Task task = list.get(getPosition());
            final String body = task.getName() + ": " + task.getTopic();
            if (list.get(getPosition()).getState().equals(State.COMPLETE)) {
                item = new CharSequence[]{
                        context.getString(R.string.edit),
                        context.getString(R.string.delete)};
            } else {
                item = new CharSequence[]{
                        context.getString(R.string.edit),
                        context.getString(R.string.delete),
                        context.getString(R.string.is_complete)};
            }

            MaterialDialog dialog = new MaterialDialog.Builder(context)
                    .title(body)
                    .items(item)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            switch (which){
                                case 0:
                                    Transaction.showFragment((ActionBarActivity) context,
                                            TaskEditFragment.getInstance(task, getPosition()));
                                    break;
                                case 1:
                                    new AlertDialogWrapper.Builder(context)
                                            .setTitle(body)
                                            .setMessage(context.getString(R.string.task_delete))
                                            .setNegativeButton(context.getString(R.string.cancel),
                                                    new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            })
                                            .setPositiveButton(context.getString(R.string.yes),
                                                    new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    new TaskService(context).delete(task.getId());
                                                    removeAt(getPosition());
                                                }
                                            }).show();
                                    break;
                                case 2:
                                    new DateFinishDialog().set(context, task, ViewHolder.this);
                                    break;
                            }
                        }
                    })
                    .show();
            TextView title = dialog.getTitleView();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(8, 8, 8, 16);
            title.setLayoutParams(params);
            title.setGravity(Gravity.CENTER);

            return true;
        }

        public void removeAt(int position) {
            list.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list.size());
        }

        public void addAt(Task task, int position) {
            list.add(position, task);
            notifyItemInserted(position);
            notifyItemRangeChanged(position, list.size());
        }

    }

    public interface OnItemLongClickListener {
        public void onLongClick(Task task);
    }
}
