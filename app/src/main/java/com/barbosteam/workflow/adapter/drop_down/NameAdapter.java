package com.barbosteam.workflow.adapter.drop_down;

import android.content.Context;
import android.widget.CheckBox;

import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.service.UserService;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.List;

/**
 * Created by Alex on 30.03.2015.
 */
public class NameAdapter extends DropDown {
    private List<User> list;

    public NameAdapter(Context context,  MaterialAutoCompleteTextView view){
        super(context, ((MainActivity)context).getFinalRecyclerAdapter().getNames(), view);
        list = new UserService(context).getAll();
    }

    public NameAdapter(Context context, MaterialAutoCompleteTextView view, CheckBox check){
        super(context, ((MainActivity)context).getFinalRecyclerAdapter().getNames(), view, check);
    }

    public User getItem(int position){
        return list.get(position);
    }

}
