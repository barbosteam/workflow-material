package com.barbosteam.workflow.adapter.drop_down;

import android.content.Context;
import android.widget.CheckBox;

import com.barbosteam.workflow.activity.MainActivity;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 30.03.2015.
 */
public class TopicAdapter extends DropDown {

    public TopicAdapter(Context context, MaterialAutoCompleteTextView view){
        super(context, ((MainActivity)context).getFinalRecyclerAdapter().getTopics(), view);
    }

    public TopicAdapter(Context context, MaterialAutoCompleteTextView view, CheckBox check){
        super(context, ((MainActivity)context).getFinalRecyclerAdapter().getTopics(), view, check);
    }

}
