package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.RecyclerAdapter;
import com.barbosteam.workflow.db.async.MainFragmentAsync;
import com.barbosteam.workflow.db.async.ProgressAsync;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.gc.materialdesign.views.ButtonFloat;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class MainFragment extends Fragment implements Initializator {

    private MainActivity context;
    private RecyclerView recyclerView;
    private View view, listEmpty;
    private ButtonFloat createTask;
    private LinkedHashMap<String, String> param;
    private RecyclerAdapter recyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, null, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();
        createAdapter();

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        context.getMenuInflater().inflate(R.menu.save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.x50:
                loadDb(50);
                return true;
            case R.id.x100:
                loadDb(100);
                return true;
            case R.id.x500:
                loadDb(500);
                return true;
            case R.id.x1000:
                loadDb(1000);
                return true;
            case R.id.x5000:
                loadDb(5000);
                return true;
            case R.id.x100000:
                loadDb(100000);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadDb(int count){
        new ProgressAsync(context, count).execute();
    }


    public void createAdapter(){
        recyclerAdapter = (context.getFinalRecyclerAdapter());
        if (recyclerAdapter == null) {
                new MainFragmentAsync(this).execute();
        } else {
            if (recyclerAdapter.getItemCount() > 0){
                recyclerView.setAdapter(recyclerAdapter);
                listEmpty.setVisibility(View.INVISIBLE);
            } else {
                listEmpty.setVisibility(View.VISIBLE);
            }
        }
    }

    public void updateAdapter(List<Task> taskList) {
        if (taskList.size() > 0) {
            recyclerAdapter = new RecyclerAdapter(context, taskList);
            recyclerView.setAdapter(recyclerAdapter);
            listEmpty.setVisibility(View.INVISIBLE);
            context.setFinalRecyclerAdapter(recyclerAdapter);

        } else {
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initializationView() {
        recyclerView = (RecyclerView) view.findViewById(R.id.task_list);
        listEmpty = view.findViewById(R.id.list_empty);
        createTask = (ButtonFloat) view.findViewById(R.id.create_task);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void initializationListeners() {
        createTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment(context, new TaskCreateFragment());
            }
        });
    }

    @Override
    public void interactionView() {

    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.main_display);
        context.setSelection(1);
    }

    @Override
    public void setAccentColor() {
        createTask.setBackgroundColor(ThemeSingleton.get().neutralColor);
    }

    public static MainFragment getInstance(LinkedHashMap<String, String> param) {
        MainFragment fragment = new MainFragment();
        fragment.param = param;
        return fragment;
    }
}
