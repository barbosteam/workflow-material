package com.barbosteam.workflow.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.UserAdapter;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.service.TaskService;
import com.barbosteam.workflow.service.UserService;
import com.gc.materialdesign.views.ButtonFloat;

import java.util.List;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserListFragment extends Fragment implements Initializator {

    private MainActivity context;
    private final int USER_EDIT = 1;
    private final int USER_DELETE = 2;

    private ListView listView;
    private View view;
    private ButtonFloat createUser;
    private int position;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_list, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        User user = (User)listView.getAdapter().getItem(position);
        menu.setHeaderTitle(user.getName());
        if (v.getId() == R.id.user_list) {
            menu.add(0, USER_EDIT, 0, R.string.edit);
            menu.add(0, USER_DELETE, 0, R.string.delete);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final User user = (User)listView.getAdapter().getItem(position);
        switch (item.getItemId()) {
            case USER_EDIT:
                Transaction.showFragment(context, UserEditFragment.getInstance(user));
                return true;
            case USER_DELETE:
                new AlertDialogWrapper.Builder(context)
                        .setTitle(user.getName())
                        .setMessage(context.getString(R.string.user_delete))
                        .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UserService userService = new UserService(context);
                                TaskService taskService = new TaskService(context);

                                //delete user
                                userService.delete(user.getId());

                                //get all tasks for this user
                                List<Task> tasks = taskService.getAll();
                                for(int i = 0; i < tasks.size(); i++){
                                    if(tasks.get(i).getIdUser() == user.getId()) {
                                        taskService.delete(tasks.get(i).getId());
                                    }
                                }

                                listView.setAdapter(new UserAdapter(new UserService(context).getAll(), context));
                                context.removeTasks();
                            }
                        }).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void initializationView() {
        listView = (ListView) view.findViewById(R.id.user_list);
        createUser = (ButtonFloat) view.findViewById(R.id.create_user);
        createUser.setBackgroundColor(getResources().getColor(R.color.indigo));
    }

    @Override
    public void initializationListeners() {
        createUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment(context, new UserAddFragment());
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserListFragment.this.position = position;
                final User user = (User)listView.getAdapter().getItem(position);
                final String body = user.getName();
                CharSequence[] item = new CharSequence[]{
                        context.getString(R.string.edit),
                        context.getString(R.string.delete)};

                MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .title(body)
                        .items(item)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                switch (which){
                                    case 0:
                                        Transaction.showFragment(context, UserEditFragment.getInstance(user));
                                        break;
                                    case 1:
                                        new AlertDialogWrapper.Builder(context)
                                                .setTitle(user.getName())
                                                .setMessage(getString(R.string.user_delete))
                                                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                })
                                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        UserService userService = new UserService(context);
                                                        TaskService taskService = new TaskService(context);

                                                        //delete user
                                                        userService.delete(user.getId());

                                                        //get all tasks for this user
                                                        List<Task> tasks = taskService.getAll();
                                                        for (int i = 0; i < tasks.size(); i++) {
                                                            if (tasks.get(i).getIdUser() == user.getId()) {
                                                                taskService.delete(tasks.get(i).getId());
                                                            }
                                                        }

                                                        listView.setAdapter(new UserAdapter(new UserService(context).getAll(), context));
                                                        context.removeTasks();
                                                    }
                                                }).show();
                                        break;
                                }
                            }
                        })
                        .show();
                TextView title = dialog.getTitleView();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(8, 8, 8, 16);
                title.setLayoutParams(params);
                title.setGravity(Gravity.CENTER);
            }
        });
    }

    @Override
    public void interactionView() {
        listView.setAdapter(new UserAdapter(new UserService(context).getAll(), context));
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.user_list);
        context.setSelection(4);
    }

    @Override
    public void setAccentColor() {
        createUser.setBackgroundColor(ThemeSingleton.get().neutralColor);
    }
}
