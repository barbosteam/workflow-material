package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.controller.ToastManager;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.service.UserService;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserEditFragment extends Fragment implements Initializator, View.OnClickListener {

    private MainActivity context;
    private View view;
    private MaterialAutoCompleteTextView name, email, phone;
    private ButtonFlat add, cancel;
    private User user;


    public static UserEditFragment getInstance(User user) {
        UserEditFragment fragment = new UserEditFragment();
        fragment.user = user;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_edit, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                user.setName(name.getText().toString());
                user.setEmail(email.getText().toString());
                user.setPhone(phone.getText().toString());
                new UserService(context).update(user);
                Transaction.showFragment(context, new UserListFragment());
                ToastManager.show(context, getString(R.string.toast_edit_user));
                break;
            case R.id.cancel:
                Transaction.showFragment(context, new UserListFragment());
                break;
        }
    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView)view.findViewById(R.id.name);
        email = (MaterialAutoCompleteTextView)view.findViewById(R.id.email);
        phone = (MaterialAutoCompleteTextView)view.findViewById(R.id.phone);

        add = (ButtonFlat)view.findViewById(R.id.add);
        cancel = (ButtonFlat)view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void interactionView() {
        name.setText(user.getName());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.user_edit);
        context.setSelection(0);
    }

    @Override
    public void setAccentColor() {
        add.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        email.setPrimaryColor(ThemeSingleton.get().neutralColor);
        phone.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }
}
