package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.drop_down.NameAdapter;
import com.barbosteam.workflow.adapter.drop_down.StateAdapter;
import com.barbosteam.workflow.adapter.drop_down.TopicAdapter;
import com.barbosteam.workflow.controller.ToastManager;
import com.barbosteam.workflow.db.resource.Resource;
import com.barbosteam.workflow.dialog.MaterialDatePicker;
import com.barbosteam.workflow.fragments.core.Emptable;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.resourse.State;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.LinkedHashMap;

/**
 * Created by Alex on 22.03.2015.
 */
public class FilterFragment extends Fragment implements Initializator, Emptable {

    private MainActivity context;
    private View view;
    private ButtonFlat find, cancel;
    private CheckBox nameCheck, topicCheck, stateCheck, dateCheck;
    private RadioButton allTask, filterTask;
    private MaterialAutoCompleteTextView name, topic, state, from, to;
    private LinearLayout focus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    @Override
    public void initializationView() {
        find = (ButtonFlat) view.findViewById(R.id.find);
        cancel = (ButtonFlat) view.findViewById(R.id.cancel);

        nameCheck = (CheckBox) view.findViewById(R.id.name_check);
        topicCheck = (CheckBox) view.findViewById(R.id.topic_check);
        stateCheck = (CheckBox) view.findViewById(R.id.state_check);
        dateCheck = (CheckBox) view.findViewById(R.id.date_check);

        allTask = (RadioButton) view.findViewById(R.id.all_task);
        filterTask = (RadioButton) view.findViewById(R.id.filter_task);

        name = (MaterialAutoCompleteTextView) view.findViewById(R.id.name);
        topic = (MaterialAutoCompleteTextView) view.findViewById(R.id.topic);
        state = (MaterialAutoCompleteTextView) view.findViewById(R.id.state);
        from = (MaterialAutoCompleteTextView) view.findViewById(R.id.from);
        to = (MaterialAutoCompleteTextView) view.findViewById(R.id.to);

        focus = (LinearLayout) view.findViewById(R.id.focus);
    }

    @Override
    public void initializationListeners() {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment(context, new MainFragment());
            }
        });
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (allTask.isChecked()) {
                    Transaction.showFragment(context, new FilterListFragment());
                }
                if (filterTask.isChecked()) {
                    if (!isEmpty()) {
                        LinkedHashMap<String, String> param = new LinkedHashMap<String, String>();
                        if (nameCheck.isChecked()) {
                            param.put(Resource.Task.NAME, name.getText().toString());
                        }
                        if (topicCheck.isChecked()) {
                            param.put(Resource.Task.TOPIC, topic.getText().toString());
                        }
                        if (stateCheck.isChecked()) {
                            switch (state.getText().toString()){
                                case State.COMPLETE_TEXT:
                                    param.put(Resource.Task.STATE, State.COMPLETE);
                                    break;
                                case State.NOT_COMPLETE_TEXT:
                                    param.put(Resource.Task.STATE, State.NOT_COMPLETE);
                                    break;
                                case State.PERFORMED_TEXT:
                                    param.put(Resource.Task.STATE, State.PERFORMED);
                                    break;
                            }
                        }
                        if (dateCheck.isChecked()) {
                            param.put("FROM", from.getText().toString());
                            param.put("TO", to.getText().toString());
                        }
                        Transaction.showFragment(context, FilterListFragment.getInstance(param));
                    } else
                        ToastManager.show(context, getString(R.string.empty_fields));
                }
            }
        });
    }

    @Override
    public void interactionView() {
        new NameAdapter(context, name, nameCheck);
        new TopicAdapter(context,  topic, topicCheck);
        new StateAdapter(context, state, stateCheck);

        new MaterialDatePicker().set(context,
                from, getString(R.string.date_from));
        new MaterialDatePicker().set(context,
                to, getString(R.string.date_to));
        setCheckLogic(nameCheck, name);
        setCheckLogic(topicCheck, topic);
        setCheckLogic(stateCheck, state);
        setCheckLogic(dateCheck, from, to);

        from.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    dateCheck.setChecked(true);
                    filterTask.setChecked(true);
                    from.performClick();
                }
            }
        });
        to.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    dateCheck.setChecked(true);
                    filterTask.setChecked(true);
                    to.performClick();
                }
            }
        });

    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.filter);
        context.setSelection(5);
    }

    @Override
    public void setAccentColor() {
        find.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        topic.setPrimaryColor(ThemeSingleton.get().neutralColor);
        state.setPrimaryColor(ThemeSingleton.get().neutralColor);
        from.setPrimaryColor(ThemeSingleton.get().neutralColor);
        to.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }

    @Override
    public boolean isEmpty() {
        return ((from.getText().toString().equals("") && dateCheck.isChecked()) ||
                (to.getText().toString().equals("") && dateCheck.isChecked()) ||
                (name.getText().toString().equals("") && nameCheck.isChecked()) ||
                (topic.getText().toString().equals("") && topicCheck.isChecked() ||
                (state.getText().toString().equals("") && stateCheck.isChecked())));
    }

    void setCheckLogic(final CheckBox check, final MaterialAutoCompleteTextView from,
                       final MaterialAutoCompleteTextView to){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked()) {
                    filterTask.setChecked(true);
                    to.requestFocus();
                    from.requestFocus();
                    focus.requestFocus();
                } else {
                    from.setText("");
                    to.setText("");
                    focus.requestFocus();
                }
                if(!nameCheck.isChecked() &&
                        !topicCheck.isChecked() &&
                        !stateCheck.isChecked() &&
                        !dateCheck.isChecked())
                    allTask.setChecked(true);
            }
        });
    }

    void setCheckLogic(final CheckBox check, final MaterialAutoCompleteTextView view){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked()) {
                    filterTask.setChecked(true);
                    view.showDropDown();
                } else {
                    view.setText("");
                    focus.requestFocus();
                }
                if(!nameCheck.isChecked() &&
                        !topicCheck.isChecked() &&
                        !stateCheck.isChecked() &&
                        !dateCheck.isChecked())
                    allTask.setChecked(true);
            }
        });
    }

}
