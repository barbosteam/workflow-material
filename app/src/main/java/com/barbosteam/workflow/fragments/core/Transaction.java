package com.barbosteam.workflow.fragments.core;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.barbosteam.workflow.R;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by Alex on 22.03.2015.
 */
public class Transaction {

    private static boolean isFirst = true;

    public static void showFragment(ActionBarActivity activity, Fragment fragment){
        showFragment(activity, fragment, R.id.container);
    }

    public static void showFragment(ActionBarActivity activity, Fragment fragment, int res){
        String backStateName = fragment.getClass().getName();
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment currentFragment = mFragmentManager.findFragmentByTag(backStateName);
        if (currentFragment != null) {
            if (!currentFragment.isVisible()) {
                ft.replace(res, fragment, backStateName);
                ft.addToBackStack(backStateName);
                ft.commit();
            }
        } else {
            ft.replace(res, fragment, backStateName);
            if (!isFirst)
                ft.addToBackStack(backStateName);
            isFirst = false;
            ft.commit();
        }
    }

    public static void replace(ActionBarActivity activity, Fragment fragment){
        String backStateName = fragment.getClass().getName();
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.replace(R.id.container, fragment, backStateName);
        if (!isFirst)
            ft.addToBackStack(backStateName);
        isFirst = false;
        ft.commit();
    }

/*
    public void showFragment(ActionBarActivity activity, Fragment fragment, int res){
        String backStateName = fragment.getClass().getName();
        Fragment backFragment = null;
        Log.e("tag", backStateName);
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment currentFragment = mFragmentManager.findFragmentByTag(backStateName);
        if (!isFirst) {
            String fragmentTag = mFragmentManager.getBackStackEntryAt(mFragmentManager.getBackStackEntryCount() - 1).getName();
            backFragment = activity.getSupportFragmentManager().findFragmentByTag(fragmentTag);
            Log.e("tag", backFragment.getClass().getName());
        }
        if (currentFragment != null) {
            Log.e("tag", currentFragment.getClass().getName());
            if (!currentFragment.isVisible()) {
                    Log.e("tag", "show");
                    ft.hide(backFragment);
                    ft.show(fragment);
                    ft.addToBackStack(backStateName);
                    ft.commit();
            }
        } else {
                Log.e("tag", "else");
                isFirst = false;
                if (backFragment != null)
                ft.hide(backFragment);
                ft.add(res, fragment, backStateName);
                ft.addToBackStack(backStateName);
                ft.commit();
        }
    }
*/

    public static void refresh(ActionBarActivity activity){
        Fragment object = null;
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment fragment = mFragmentManager.findFragmentById(R.id.container);
        try {
            object = (Fragment) Class.forName(fragment.getClass().getName()).getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        replace(activity, object);
    }

    public static void back(ActionBarActivity activity){
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        mFragmentManager.popBackStack();
    }

}
