package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.drop_down.NameAdapter;
import com.barbosteam.workflow.adapter.drop_down.TopicAdapter;
import com.barbosteam.workflow.controller.ToastManager;
import com.barbosteam.workflow.dialog.MaterialDatePicker;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.service.TaskService;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 22.03.2015.
 */
public class TaskEditFragment extends Fragment implements Initializator, View.OnClickListener {

    private MainActivity context;
    private View view;
    private MaterialAutoCompleteTextView name, dateComplete, topic, comment;
    private ButtonFlat save, cancel;
    private int adapterPosition;
    private Task task;

    public static TaskEditFragment getInstance(Task task, int adapterPosition) {
        TaskEditFragment fragment = new TaskEditFragment();
        fragment.task = task;
        fragment.adapterPosition = adapterPosition;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_task_edit, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                task.setName(name.getText().toString());
                task.setDateComplete(dateComplete.getText().toString());
                task.setTopic(topic.getText().toString());
                task.setCommentYou(comment.getText().toString());

                new TaskService(context).update(task);
                context.removeItem(adapterPosition);
                context.addItem(task, adapterPosition);

                Transaction.showFragment(context, new MainFragment());
                ToastManager.show(context, getString(R.string.toast_edit_task));
                break;
            case R.id.cancel:
                Transaction.showFragment(context, new MainFragment());
                break;
        }

    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView)view.findViewById(R.id.name);
        dateComplete = (MaterialAutoCompleteTextView)view.findViewById(R.id.date_complete);
        topic = (MaterialAutoCompleteTextView)view.findViewById(R.id.topic);
        comment = (MaterialAutoCompleteTextView)view.findViewById(R.id.comment);
        save = (ButtonFlat)view.findViewById(R.id.save);
        cancel = (ButtonFlat)view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        save.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void interactionView() {
        new NameAdapter(context,  name);
        new TopicAdapter(context,  topic);

        name.setText(task.getName());
        dateComplete.setText(task.getDateComplete());
        topic.setText(task.getTopic());
        comment.setText(task.getCommentYou());

        new MaterialDatePicker().set(context,
                dateComplete, getString(R.string.date_complete));
        comment.setMaxCharacters(100);
        topic.setMaxCharacters(15);
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.task_edit);
        context.setSelection(0);
    }

    @Override
    public void setAccentColor() {
        save.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        topic.setPrimaryColor(ThemeSingleton.get().neutralColor);
        dateComplete.setPrimaryColor(ThemeSingleton.get().neutralColor);
        comment.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }
}
