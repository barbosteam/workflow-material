package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.controller.ToastManager;
import com.barbosteam.workflow.fragments.core.Emptable;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.service.UserService;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.regex.Pattern;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserAddFragment extends Fragment implements Initializator, Emptable, View.OnClickListener {

    private MainActivity context;
    private View view;
    private MaterialAutoCompleteTextView name, email, phone;
    private ButtonFlat add, cancel;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_add, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                if(!isEmpty()) {
                    UserService service = new UserService(context);
                    User newUser = new User(name.getText().toString().trim(),
                            email.getText().toString().trim().toLowerCase(),
                            phone.getText().toString().trim());
                    if(emailCheck()) {
                        if(!service.exist(newUser)) {
                            service.save(newUser);
                            Transaction.replace(context, new UserAddFragment());
                            ToastManager.show(context, getString(R.string.toast_add_user));
                        } else {
                            ToastManager.show(context, getString(R.string.user_exist));
                        }
                    } else {
                        ToastManager.show(context, getString(R.string.incorrect_email));
                    }
                } else {
                    ToastManager.show(context, getString(R.string.empty_fields));
                }
                break;
            case R.id.cancel:
                Transaction.showFragment(context, new MainFragment());
                break;
        }
    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView)view.findViewById(R.id.name);
        email = (MaterialAutoCompleteTextView)view.findViewById(R.id.email);
        phone = (MaterialAutoCompleteTextView)view.findViewById(R.id.phone);
		
        add = (ButtonFlat)view.findViewById(R.id.add);
        cancel = (ButtonFlat)view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void interactionView() {

    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.user_add);
        context.setSelection(3);
    }

    @Override
    public void setAccentColor() {
        add.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
		
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        email.setPrimaryColor(ThemeSingleton.get().neutralColor);
        phone.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }

    @Override
    public boolean isEmpty() {
        return  name.getText().toString().trim().equals("") ||
                email.getText().toString().trim().equals("") ||
                phone.getText().toString().trim().length() == 0;
    }

    private boolean emailCheck() {
        Pattern pat = Pattern.compile("^[a-z]+[a-z0-9]*(([._-][a-z0-9]+)|([a-z0-9]*))@[a-z]{2,}.[a-z]{2,}(.[a-z]{2,})?$");
        return pat.matcher(email.getText().toString().trim().toLowerCase()).find();
    }
}
