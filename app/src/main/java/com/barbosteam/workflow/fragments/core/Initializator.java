package com.barbosteam.workflow.fragments.core;

/**
 * Created by Alex on 27.03.2015.
 */
public interface Initializator {
    void initializationView();
    void initializationListeners();
    void interactionView();
    void setActionBarParam();
    void setAccentColor();
}
