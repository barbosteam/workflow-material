package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.RecyclerAdapter;
import com.barbosteam.workflow.db.async.FilterFragmentAsync;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.gc.materialdesign.views.ButtonFloat;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class FilterListFragment extends Fragment implements Initializator {

    private MainActivity context;
    private RecyclerAdapter filterAdapter;
    private RecyclerView recyclerView;
    private View view, listEmpty;
    private ButtonFloat filter;
    private LinkedHashMap<String, String> param;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter_list, null, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();
        createAdapter();

        return view;
    }

    public void createAdapter(){
            new FilterFragmentAsync(this, param).execute();
    }

    public void updateAdapter(List<Task> taskList) {
        if (taskList.size() > 0) {
            filterAdapter = new RecyclerAdapter(context, taskList);
            recyclerView.setAdapter(filterAdapter);
            listEmpty.setVisibility(View.GONE);
        } else {
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initializationView() {
        recyclerView = (RecyclerView) view.findViewById(R.id.task_list);
        listEmpty = view.findViewById(R.id.list_empty);
        filter = (ButtonFloat) view.findViewById(R.id.filter);
        filter.setBackgroundColor(getResources().getColor(R.color.indigo));
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void initializationListeners() {
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment(context, new FilterFragment());
            }
        });
    }

    @Override
    public void interactionView() {

    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.filter_list);
        context.setSelection(5);
    }

    @Override
    public void setAccentColor() {
        filter.setBackgroundColor(ThemeSingleton.get().neutralColor);
    }

    public static FilterListFragment getInstance(LinkedHashMap<String, String> param) {
        FilterListFragment fragment = new FilterListFragment();
        fragment.param = param;
        return fragment;
    }
}
