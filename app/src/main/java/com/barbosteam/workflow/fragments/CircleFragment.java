package com.barbosteam.workflow.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.gc.materialdesign.views.ButtonFloat;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by Alex on 22.03.2015.
 */

public class CircleFragment extends Fragment implements Initializator {

    private MainActivity context;
    private PieChart mChart;
    private View view;
    private int[] statistics;
    private ButtonFloat statistic;
    private String title;
    private TextView titleView;

    protected String[] state = new String[]{
            getString(R.string.complete),
            getString(R.string.not_complete),
            getString(R.string.performed)
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_circle, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    public static CircleFragment getInstance(int[] statistics, String title) {
        CircleFragment fragment = new CircleFragment();
        fragment.statistics = statistics;
        fragment.title = title;
        return fragment;
    }

    private void setData(int[] statistics) {

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        for (int i = 0; i < statistics.length; i++) {
            if (statistics[i] != 0)
                yVals1.add(new Entry(statistics[i], i));
        }

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < statistics.length; i++) {
            if (statistics[i] != 0)
                xVals.add(state[i]
                        + ": " + String.valueOf(statistics[i]));
        }

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        ArrayList<Integer> colors = new ArrayList<Integer>();
        if (statistics[0] != 0)
        colors.add(Color.parseColor("#4CAF50"));
        if (statistics[1] != 0)
        colors.add(Color.parseColor("#F44336"));
        if (statistics[2] != 0)
        colors.add(Color.parseColor("#CDDC39"));

        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.WHITE);
        mChart.setData(data);

        mChart.highlightValues(null);
        mChart.invalidate();
    }

    @Override
    public void initializationView() {
        statistic = (ButtonFloat) view.findViewById(R.id.create_task);
        mChart = (PieChart) view.findViewById(R.id.chart1);
        titleView = (TextView) view.findViewById(R.id.title);
    }

    @Override
    public void initializationListeners() {
        statistic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment((MainActivity) context, new StatisticFragment());
            }
        });
    }

    @Override
    public void interactionView() {
        mChart.setUsePercentValues(true);
        mChart.setNoDataText(getString(R.string.list_filter_empty));
        mChart.setDrawCenterText(false);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);
        mChart.setHoleRadius(35f);
        mChart.setTransparentCircleRadius(0f);
        mChart.setRotationAngle(0);
        mChart.setDescription("");
        titleView.setText(title);

        mChart.setRotationEnabled(true);
        mChart.setDrawSliceText(false);

        setData(statistics);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setFormSize(14f);
        l.setTextSize(16f);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(8f);
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.statistic);
        context.setSelection(6);
    }

    @Override
    public void setAccentColor() {
        statistic.setBackgroundColor(ThemeSingleton.get().neutralColor);
    }

}