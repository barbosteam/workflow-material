package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.drop_down.NameAdapter;
import com.barbosteam.workflow.adapter.drop_down.TopicAdapter;
import com.barbosteam.workflow.controller.ToastManager;
import com.barbosteam.workflow.dialog.core.DateToday;
import com.barbosteam.workflow.dialog.MaterialDatePicker;
import com.barbosteam.workflow.fragments.core.Emptable;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.resourse.State;
import com.barbosteam.workflow.service.TaskService;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 22.03.2015.
 */
public class TaskCreateFragment extends Fragment implements Initializator, Emptable {

    private MainActivity context;
    private View view;
    private MaterialAutoCompleteTextView name, dateComplete, topic, comment;
    private ButtonFlat save, cancel;
    private NameAdapter nameAdapter;
    private int position;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_task_create, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView) view.findViewById(R.id.name);
        dateComplete = (MaterialAutoCompleteTextView) view.findViewById(R.id.date_complete);
        topic = (MaterialAutoCompleteTextView) view.findViewById(R.id.topic);
        comment = (MaterialAutoCompleteTextView) view.findViewById(R.id.comment);

        save = (ButtonFlat) view.findViewById(R.id.save);
        cancel = (ButtonFlat) view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {

        name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TaskCreateFragment.this.position = position;
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty()) {
                    Task task = new Task(
                            nameAdapter.getItem(position).getId(),
                            nameAdapter.getItem(position).getName(),
                            DateToday.getDate(),
                            dateComplete.getText().toString(),
                            null,
                            topic.getText().toString(),
                            comment.getText().toString(),
                            null,
                            State.PERFORMED
                    );
                    long id = new TaskService(context).save(task);
                    task.setId(id);
                    context.addItem(task, 0);
                    Transaction.showFragment(context, new MainFragment());
                    ToastManager.show(context, getString(R.string.toast_create_task));
                } else
                    ToastManager.show(context, getString(R.string.empty_fields));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment(context, new MainFragment());
            }
        });

    }

    @Override
    public void interactionView() {
        nameAdapter = new NameAdapter(context,  name);
        new TopicAdapter(context,  topic);
        //new DropDownAsync(getActivity(), topic).execute();

        new MaterialDatePicker().set(context,
                dateComplete, getString(R.string.date_complete));
        comment.setMaxCharacters(100);
        topic.setMaxCharacters(15);
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.task_create);
        context.setSelection(2);
    }

    @Override
    public void setAccentColor() {
        save.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        topic.setPrimaryColor(ThemeSingleton.get().neutralColor);
        dateComplete.setPrimaryColor(ThemeSingleton.get().neutralColor);
        comment.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }

    @Override
    public boolean isEmpty() {
        return name.getText().toString().equals("")
                || topic.getText().toString().equals("")
                || dateComplete.getText().toString().equals("")
                || comment.getText().toString().equals("");
    }

}
