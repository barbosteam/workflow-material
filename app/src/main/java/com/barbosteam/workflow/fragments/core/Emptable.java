package com.barbosteam.workflow.fragments.core;

/**
 * Created by Alex on 27.03.2015.
 */
public interface Emptable {
    boolean isEmpty();
}
