package com.barbosteam.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.ThemeSingleton;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.drop_down.NameAdapter;
import com.barbosteam.workflow.db.resource.Resource;
import com.barbosteam.workflow.dialog.MaterialDatePicker;
import com.barbosteam.workflow.fragments.core.Initializator;
import com.barbosteam.workflow.fragments.core.Transaction;
import com.barbosteam.workflow.service.TaskService;
import com.gc.materialdesign.views.ButtonFlat;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.LinkedHashMap;

/**
 * Created by Alex on 22.03.2015.
 */
public class StatisticFragment extends Fragment implements Initializator {

    private MainActivity context;
    private View view;
    private MaterialAutoCompleteTextView name, from, to;
    private ButtonFlat find, cancel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_statictic, container, false);
        this.context = (MainActivity) getActivity();

        setActionBarParam();
        initializationView();
        initializationListeners();
        interactionView();
        setAccentColor();

        return view;
    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView) view.findViewById(R.id.name);
        from = (MaterialAutoCompleteTextView) view.findViewById(R.id.from);
        to = (MaterialAutoCompleteTextView) view.findViewById(R.id.to);

        find = (ButtonFlat) view.findViewById(R.id.find);
        cancel = (ButtonFlat) view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkedHashMap<String, String> args = new LinkedHashMap<>();
                String nameText = name.getText().toString();
                String fromText = from.getText().toString();
                String toText = to.getText().toString();

                if(!nameText.equals("")) {
                    args.put(Resource.Task.NAME, nameText);
                }
                if(!fromText.equals("")) {
                    args.put("FROM", fromText);
                }
                if(!toText.equals("")) {
                    args.put("TO", toText);
                }
                String title = "";
                if (args.size() > 0  && !nameText.equals("")) {
                    title = getString(R.string.statistic) + " " + nameText;
                    if (!fromText.equals(""))
                        title = title + getString(R.string.spell_with) + fromText;
                    if (!toText.equals("")){
                        if (!fromText.equals(""))
                            title = title + getString(R.string.to_little) + toText;
                        else
                            title = title + getString(R.string.spell_to) + toText;
                    }

                } else {
                    title = getString(R.string.all_statistic);
                    if (!fromText.equals(""))
                        title = title + getString(R.string.spell_with) + fromText;
                    if (!toText.equals("")) {
                        if (!fromText.equals(""))
                            title = title + getString(R.string.to_little) + toText;
                        else
                            title = title + getString(R.string.spell_to) + toText;
                    }
                }
                name.setText(null);
                from.setText(null);
                to.setText(null);
                Transaction.showFragment(context,
                        CircleFragment.getInstance(new TaskService(context).getStatisticList(args), title));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.back(context);
            }
        });
    }

    @Override
    public void interactionView() {
        new NameAdapter(context,  name);
        new MaterialDatePicker().set(context,
                from, getString(R.string.date_from));
        new MaterialDatePicker().set(context,
                to, getString(R.string.date_to));
    }

    @Override
    public void setActionBarParam() {
        context.getSupportActionBar().setTitle(R.string.statistic);
        context.setSelection(6);
    }

    @Override
    public void setAccentColor() {
        find.setBackgroundColor(ThemeSingleton.get().neutralColor);
        cancel.setBackgroundColor(ThemeSingleton.get().neutralColor);
        name.setPrimaryColor(ThemeSingleton.get().neutralColor);
        from.setPrimaryColor(ThemeSingleton.get().neutralColor);
        to.setPrimaryColor(ThemeSingleton.get().neutralColor);
    }
}