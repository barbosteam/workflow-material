package com.barbosteam.workflow.controller;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Alex on 31.03.2015.
 */
public class ToastManager {

    public static void show(Context context, String body){
        Toast.makeText(context, body, Toast.LENGTH_SHORT).show();
    }

}
