package com.barbosteam.workflow.service;

import android.content.Context;

import com.barbosteam.workflow.db.dao.TaskDao;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.service.core.OpenBDService;
import com.barbosteam.workflow.service.core.Service;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public class TaskService extends OpenBDService implements Service<Task> {
    Context context;

    public TaskService(Context context) {
        this.context = context;
    }

    @Override
    public long save(Task task) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).save(task);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public long delete(Task task) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).delete(task);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public long delete(long id) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).delete(id);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public int update(Task task) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).update(task);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public List<Task> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).getAll();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public Task getById(Task task) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).getById(task.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public Task getById(long id) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).getById(id);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public List<Task> getFilterList(LinkedHashMap<String, String> param) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).getFilterList(param);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public int[] getStatisticList(LinkedHashMap<String, String> param) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new TaskDao(getSqLiteDatabase()).getStatisticList(param);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

}
