package com.barbosteam.workflow.service;

import android.content.Context;

import com.barbosteam.workflow.db.dao.UserDao;
import com.barbosteam.workflow.model.User;
import com.barbosteam.workflow.service.core.OpenBDService;
import com.barbosteam.workflow.service.core.Service;

import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public class UserService extends OpenBDService implements Service<User> {
    Context context;

    public UserService(Context context) {
        this.context = context;
    }

    @Override
    public long save(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).save(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public long delete(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).delete(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public long delete(long id) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).delete(id);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public int update(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).update(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public List<User> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).getAll();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public User getById(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).getById(user.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    @Override
    public User getById(long id) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).getById(id);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public boolean exist(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).exist(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

}
