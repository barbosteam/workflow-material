package com.barbosteam.workflow.service.core;

import java.util.List;

/**
 * Created by Alex on 23.03.2015.
 */
public interface Service<T> {

    long save(T t);

    long delete(T t);

    long delete(long id);

    int update(T t);

    List<T> getAll();

    T getById(T t);

    T getById(long id);

}
