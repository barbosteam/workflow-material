package com.barbosteam.workflow.service.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.barbosteam.workflow.db.DBHelper;

/**
 * Created by Alex on 23.03.2015.
 */
public abstract class OpenBDService {

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }

    protected boolean isOpenDB() {
        return sqLiteDatabase != null && dbHelper != null && sqLiteDatabase.isOpen();
    }

    protected void open(Context mContext) {
        if (sqLiteDatabase == null || !sqLiteDatabase.isOpen()) {
            dbHelper = new DBHelper(mContext);
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
    }

    protected void close() {
        if (dbHelper != null) {
            dbHelper.close();

        }
    }

}
