package com.barbosteam.workflow.model.resourse;

import java.util.ArrayList;

/**
 * Created by Alex on 23.03.2015.
 */
public class State {
    public static final String COMPLETE = "complete";
    public static final String NOT_COMPLETE = "not_complete";
    public static final String PERFORMED = "performed";

    public static final String COMPLETE_TEXT = "Виконано";
    public static final String NOT_COMPLETE_TEXT = "Не виконано";
    public static final String PERFORMED_TEXT = "Виконується";
    public static final ArrayList<String> STATE_LIST = new ArrayList<String>();;
    static {
        STATE_LIST.add(COMPLETE_TEXT);
        STATE_LIST.add(NOT_COMPLETE_TEXT);
        STATE_LIST.add(PERFORMED_TEXT);
    }
}
