package com.barbosteam.workflow.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex on 23.03.2015.
 */
public class Task implements Parcelable {

    private long id;
    private long idUser;
    private String name;
    private String dateCreate;
    private String dateComplete;
    private String dateFinish;
    private String topic;
    private String commentYou;
    private String commentUser;
    private String state;

    public Task(long id, long idUser, String name, String dateCreate, String dateComplete,
                String dateFinish, String topic, String commentYou, String commentUser, String state){
        this.id = id;
        this.idUser = idUser;
        this.name = name;
        this.dateCreate = dateCreate;
        this.dateComplete = dateComplete;
        this.dateFinish = dateFinish;
        this.topic = topic;
        this.commentYou = commentYou;
        this.commentUser = commentUser;
        this.state = state;
    }

    public Task(long idUser, String name, String dateCreate, String dateComplete, String dateFinish,
                String topic, String commentYou, String commentUser, String state){
        this.idUser = idUser;
        this.name = name;
        this.dateCreate = dateCreate;
        this.dateComplete = dateComplete;
        this.dateFinish = dateFinish;
        this.topic = topic;
        this.commentYou = commentYou;
        this.commentUser = commentUser;
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateComplete() {
        return dateComplete;
    }

    public void setDateComplete(String dateComplete) {
        this.dateComplete = dateComplete;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getCommentYou() {
        return commentYou;
    }

    public void setCommentYou(String commentYou) {
        this.commentYou = commentYou;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(String commentUser) {
        this.commentUser = commentUser;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(idUser);
        dest.writeString(name);
        dest.writeString(dateCreate);
        dest.writeString(dateComplete);
        dest.writeString(dateFinish);
        dest.writeString(topic);
        dest.writeString(commentYou);
        dest.writeString(commentUser);
        dest.writeString(state);
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    private Task(Parcel parcel) {
        id = parcel.readLong();
        idUser = parcel.readLong();
        name = parcel.readString();
        dateCreate = parcel.readString();
        dateComplete = parcel.readString();
        dateFinish = parcel.readString();
        topic = parcel.readString();
        commentYou = parcel.readString();
        commentUser = parcel.readString();
        state = parcel.readString();
    }
}
