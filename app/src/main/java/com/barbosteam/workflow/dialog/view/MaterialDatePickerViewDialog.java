package com.barbosteam.workflow.dialog.view;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.RecyclerAdapter;
import com.barbosteam.workflow.model.Task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Alex on 12.04.2015.
 */
public class MaterialDatePickerViewDialog {

    private MainActivity context;
    private Task task;
    private RecyclerAdapter.ViewHolder viewHolder;
    private String date;
    private Calendar calendar;
    private NumberPicker day, month, year;
    private String[] monthDisplay;

    public MaterialDatePickerViewDialog(MainActivity context, String date){
        this.context = context;
        this.date = date;
        this.monthDisplay = context.getResources().getStringArray(R.array.month);
    }

    public View getView(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.from(context).inflate(R.layout.dialog_date_picker, null, false);
        day = (NumberPicker) view.findViewById(R.id.day);
        month = (NumberPicker) view.findViewById(R.id.month);
        year = (NumberPicker) view.findViewById(R.id.year);
        LinearLayout focus = (LinearLayout) view.findViewById(R.id.focus);
        focus.requestFocus();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        day.setMinValue(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        day.setMaxValue(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        month.setMinValue(calendar.getMinimum(Calendar.MONTH));
        month.setMaxValue(calendar.getMaximum(Calendar.MONTH));
        year.setMinValue(calendar.getMinimum(Calendar.YEAR));
        year.setMaxValue(calendar.getMaximum(Calendar.YEAR));

        updatePicker();
        setDividerColor(day);
        setDividerColor(month);
        setDividerColor(year);

        month.setDisplayedValues(monthDisplay);


        day.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                calendar.set(Calendar.DAY_OF_MONTH, newVal);
                if (oldVal > newVal + 20) {
                    calendar.add(Calendar.MONTH, 1);
                    updatePicker();
                }
                if (oldVal < newVal -20) {
                    calendar.add(Calendar.MONTH, -1);
                    updatePicker();
                }

            }
        });

        month.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                calendar.set(Calendar.MONTH, newVal);
                if (oldVal > newVal + 10) {
                    calendar.add(Calendar.YEAR, 1);
                    updatePicker();
                }
                if (oldVal < newVal -10) {
                    calendar.add(Calendar.YEAR, -1);
                    updatePicker();
                }
                updateDay();
            }
        });

        year.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                calendar.set(Calendar.YEAR, newVal);
                updateDay();
            }
        });

        return view;
    }


    public void updateDay(){
        day.setMaxValue(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    }

    public void updatePicker(){
        day.setMaxValue(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        day.setValue(calendar.get(Calendar.DAY_OF_MONTH));
        month.setValue(calendar.get(Calendar.MONTH));
        year.setValue(calendar.get(Calendar.YEAR));
    }

    public String getSetDate(){
        int year = calendar.get(Calendar.YEAR);
        String dayOfMonthS = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String monthOfYearS = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (dayOfMonthS.length() == 1) dayOfMonthS = "0" + dayOfMonthS;
        if (monthOfYearS.length() == 1) monthOfYearS = "0" + monthOfYearS;
        return dayOfMonthS + "." + monthOfYearS + "." + year;
    }

    private void setDividerColor (NumberPicker picker) {
        int drawable = 0;

        switch (context.getIndex()){
            case 0:
                drawable = R.drawable.np_numberpicker_selection_divider_app;
                break;
            case 1:
                drawable = R.drawable.np_numberpicker_selection_divider_1;
                break;
            case 2:
                drawable = R.drawable.np_numberpicker_selection_divider_2;
                break;
            case 3:
                drawable = R.drawable.np_numberpicker_selection_divider_3;
                break;
            case 4:
                drawable = R.drawable.np_numberpicker_selection_divider_4;
                break;
            case 5:
                drawable = R.drawable.np_numberpicker_selection_divider_5;
                break;
            case 6:
                drawable = R.drawable.np_numberpicker_selection_divider_6;
                break;
            case 7:
                drawable = R.drawable.np_numberpicker_selection_divider_7;
                break;
            case 8:
                drawable = R.drawable.np_numberpicker_selection_divider_8;
                break;
            case 9:
                drawable = R.drawable.np_numberpicker_selection_divider_9;
                break;
            case 10:
                drawable = R.drawable.np_numberpicker_selection_divider_10;
                break;
            case 11:
                drawable = R.drawable.np_numberpicker_selection_divider_11;
                break;
            case 12:
                drawable = R.drawable.np_numberpicker_selection_divider_12;
                break;
            case 13:
                drawable = R.drawable.np_numberpicker_selection_divider_13;
                break;
            case 14:
                drawable = R.drawable.np_numberpicker_selection_divider_14;
                break;
            case 15:
                drawable = R.drawable.np_numberpicker_selection_divider_15;
                break;
            case 16:
                drawable = R.drawable.np_numberpicker_selection_divider_16;
                break;
            case 17:
                drawable = R.drawable.np_numberpicker_selection_divider_17;
                break;
            case 18:
                drawable = R.drawable.np_numberpicker_selection_divider_18;
                break;
            case 19:
                drawable = R.drawable.np_numberpicker_selection_divider_19;
                break;
            case 20:
                drawable = R.drawable.np_numberpicker_selection_divider_20;
                break;
        }

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    pf.set(picker, context.getResources().getDrawable(drawable));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        //}
    }
}
