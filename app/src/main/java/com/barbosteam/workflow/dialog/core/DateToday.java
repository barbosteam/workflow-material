package com.barbosteam.workflow.dialog.core;

import java.util.Calendar;

/**
 * Created by Alex on 28.03.2015.
 */
public class DateToday {

    public static String getDate(){
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        String dayOfMonthS = String.valueOf(mcurrentTime.get(Calendar.DAY_OF_MONTH));
        String monthOfYearS = String.valueOf(mcurrentTime.get(Calendar.MONTH) + 1);
        if (dayOfMonthS.length() == 1) dayOfMonthS = "0" + dayOfMonthS;
        if (monthOfYearS.length() == 1) monthOfYearS = "0" + monthOfYearS;
        return dayOfMonthS + "." + monthOfYearS + "." + year;
    }

}
