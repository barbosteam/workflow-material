package com.barbosteam.workflow.dialog;

import android.content.DialogInterface;
import android.view.View;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.adapter.RecyclerAdapter;
import com.barbosteam.workflow.dialog.core.DateToday;
import com.barbosteam.workflow.dialog.view.MaterialDatePickerViewDialog;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.resourse.State;
import com.barbosteam.workflow.service.TaskService;

/**
 * Created by Alex on 28.03.2015.
 */
public class DateFinishDialog {

    private Task task;
    private RecyclerAdapter.ViewHolder viewHolder;
    private MaterialDatePickerViewDialog viewDialog;
    private View view;
    private MainActivity context;

    public void set(MainActivity context, Task task, RecyclerAdapter.ViewHolder viewHolder){
        this.context = context;
        this.task = task;
        this.viewHolder = viewHolder;
        this.context = context;
        show();
    }

    public void show(){
        viewDialog = new MaterialDatePickerViewDialog(context, DateToday.getDate());
        view = viewDialog.getView();
        new AlertDialogWrapper.Builder(context)
                .setPositiveButton(context.getString(R.string.apcept),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                task.setState(State.COMPLETE);
                                task.setDateFinish(viewDialog.getSetDate());
                                new TaskService(context).update(task);
                                viewHolder.removeAt(viewHolder.getPosition());
                                viewHolder.addAt(task, viewHolder.getPosition());
                            }
                        })
                .setNegativeButton(context.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .setView(view).setTitle(context.getString(R.string.date_finish_title)).show();
    }

}
