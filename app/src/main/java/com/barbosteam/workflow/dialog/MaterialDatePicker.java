package com.barbosteam.workflow.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.barbosteam.workflow.R;
import com.barbosteam.workflow.activity.MainActivity;
import com.barbosteam.workflow.dialog.core.DateToday;
import com.barbosteam.workflow.dialog.view.MaterialDatePickerViewDialog;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 14.04.2015.
 */
public class MaterialDatePicker {

    private MaterialDatePickerViewDialog viewDialog;
    private View view;
    private MainActivity context;
    private String date, title;
    private MaterialAutoCompleteTextView text;

    public void set(MainActivity context, MaterialAutoCompleteTextView text, String title){
        this.context = context;
        this.text = text;
        this.title = title;
        updateDate();
        setShowDialogListener();
    }

    public AlertDialog getNewDialog(){
        viewDialog = new MaterialDatePickerViewDialog(context, date);
        view = viewDialog.getView();
        AlertDialog newDialog = new AlertDialogWrapper.Builder(context)
                .setPositiveButton(context.getString(R.string.apcept),
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        text.setText(viewDialog.getSetDate());
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setView(view).setTitle(title).show();
        return newDialog;
    }

    public void updateDate(){
        if (!text.getText().toString().equals("")){
            this.date = text.getText().toString();
        } else
            this.date = DateToday.getDate();
    }

    public void setShowDialogListener(){
        text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    updateDate();
                    getNewDialog();
                }
            }
        });
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate();
                getNewDialog();
            }
        });
    }



}
