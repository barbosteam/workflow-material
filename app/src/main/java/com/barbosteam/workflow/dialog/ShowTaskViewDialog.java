package com.barbosteam.workflow.dialog;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;

import com.barbosteam.workflow.R;
import com.barbosteam.workflow.model.Task;
import com.barbosteam.workflow.model.resourse.State;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 12.04.2015.
 */
public class ShowTaskViewDialog {

    private Context context;
    private Task task;

    public ShowTaskViewDialog(Context context, Task task){
        this.context = context;
        this.task = task;
    }

    public View getView(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.from(context).inflate(R.layout.fragment_show_task, null, false);

        MaterialAutoCompleteTextView name = (MaterialAutoCompleteTextView) view.findViewById(R.id.name);
        MaterialAutoCompleteTextView dateComplete = (MaterialAutoCompleteTextView) view.findViewById(R.id.date_complete);
        MaterialAutoCompleteTextView dateCreate = (MaterialAutoCompleteTextView) view.findViewById(R.id.date_create);
        MaterialAutoCompleteTextView dateFinish = (MaterialAutoCompleteTextView) view.findViewById(R.id.date_finish);
        MaterialAutoCompleteTextView topic = (MaterialAutoCompleteTextView) view.findViewById(R.id.topic);
        MaterialAutoCompleteTextView state = (MaterialAutoCompleteTextView) view.findViewById(R.id.state);
        MaterialAutoCompleteTextView comment = (MaterialAutoCompleteTextView) view.findViewById(R.id.comment);



        name.setTextColor(Color.BLACK);
        name.setFloatingLabelTextColor(Color.BLACK);
        dateComplete.setTextColor(Color.BLACK);
        dateComplete.setFloatingLabelTextColor(Color.BLACK);
        topic.setTextColor(Color.BLACK);
        topic.setFloatingLabelTextColor(Color.BLACK);
        comment.setTextColor(Color.BLACK);
        comment.setFloatingLabelTextColor(Color.BLACK);
        dateCreate.setTextColor(Color.BLACK);
        dateCreate.setFloatingLabelTextColor(Color.BLACK);
        dateFinish.setTextColor(Color.BLACK);
        dateFinish.setFloatingLabelTextColor(Color.BLACK);
        state.setTextColor(Color.BLACK);
        state.setFloatingLabelTextColor(Color.BLACK);

        name.setText(task.getName());
        dateComplete.setText(task.getDateComplete());
        dateCreate.setText(task.getDateCreate());
        topic.setText(task.getTopic());
        comment.setText(task.getCommentYou());

        switch (task.getState()){
            case State.COMPLETE:
                state.setText(context.getString(R.string.complete));
                dateFinish.setText(task.getDateFinish());
                break;
            case State.NOT_COMPLETE:
                state.setText(context.getString(R.string.not_complete));
                break;
            case State.PERFORMED:
                state.setText(context.getString(R.string.performed));
                break;
        }

        return view;
    }
}